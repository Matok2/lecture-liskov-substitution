<?php

namespace Matok;

class Square extends Rectangle
{
    public function __construct($width)
    {
        $this->setWidth($width);
        $this->setHeight($width);
    }

    public function setHeight($height)
    {
        parent::setWidth($height);
        parent::setHeight($height);
    }

    public function setWidth($width)
    {
        parent::setWidth($width);
        parent::setHeight($width);
    }
}