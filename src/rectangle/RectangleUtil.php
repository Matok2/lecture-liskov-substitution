<?php

namespace Matok;

class RectangleUtil
{
    public static function doubleHeight(Rectangle $rectangle)
    {
        $rectangle->setHeight(2*$rectangle->getHeight());
    }

    public static function printRectangle(Rectangle $rectangle)
    {
        echo "a: {$rectangle->getWidth()}, b: {$rectangle->getHeight()}, surface: {$rectangle->surface()}";
    }
}