# Lecture: Liskov Substitution Principle

## Install
```
composer install
```

## Run
```
php rectangle.php
```

## Task #1
1. Examine Rectangle.php
2. Examine utility class RectangleUtil.php that operate on **Rectangle** class
3. Examine Square.php ( **Square** extends **Rectangle** )
4. Examine output from rectangle.php script - why is LSP violated?