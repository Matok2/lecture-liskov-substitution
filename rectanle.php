<?php
include __DIR__ . '/vendor/autoload.php';

use Matok\Rectangle;
use Matok\Square;
use Matok\RectangleUtil;

$r1 = new Rectangle(2, 4);

$r2 = new Rectangle(2, 1);

$s1 = new Square(10);

RectangleUtil::printRectangle($r1);
echo "\n\n";
RectangleUtil::printRectangle($r2);
echo "\n\n";
RectangleUtil::printRectangle($s1);
echo "\n\n";

echo "Double rectangle height...";
RectangleUtil::doubleHeight($r1);
RectangleUtil::doubleHeight($r2);
RectangleUtil::doubleHeight($s1);
echo "\n\n";

echo "2x height means 2x surface:\n";
RectangleUtil::printRectangle($r1);
echo "\n\n";
RectangleUtil::printRectangle($r2);
echo "\n\n";
RectangleUtil::printRectangle($s1);
echo "\n\n";
echo "                        ^\n";
echo "                        |\n";
echo "                        |\n";
echo "                    Wot??\n";
